

<?php
require 'includes/common.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="node_modules/bootstrap-social/bootstrap-social.css">
    <link href="./node_modules/font-awesome/css/all.css" rel="stylesheet">
  <link href="./node_modules/font-awesome/css/fontawesome.css" rel="stylesheet">
  <link href="./node_modules/font-awesome/css/brands.css" rel="stylesheet">
  <link href="./node_modules/font-awesome/css/solid.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon_io/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon_io/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon_ioo/favicon-16x16.png">
<link rel="manifest" href="favicon_io/site.webmanifest">
    <title> Code@Random-Terms and Conditions</title>
</head>
<body>

<!--Login Modal-->
<?php include 'includes/login.php';?>
<!--SignUp Modal-->
<?php include 'includes/signup.php';?>
<nav class="navbar navbar-dark navbar-expand-sm fixed-top" id="navbarcr">
    <div class="container">
        <a class="navbar-brand col-sm-6" href="index.php"><img src="images/LOGO.png" height="50" width="50"></a>
</div>
</nav>
    <body style="font-family: Arial; line-height: 1.4em; padding-top:100px;">
    <h4 class="mt-2" style="justify-content: center;"> Refund Policy</h4>
     
    <br><br>
     
    
    Returns
    <br><br>
    Our policy lasts 7 days. If 7 days have gone by since your purchase, unfortunately we can’t offer you a refund or exchange.
    <br><br>
    If an enrolment is requested for any Course via the Site, Application or Services, We will either pre-approve, confirm or reject the enrolment request within the period of 7 days from the date of request for enrolment (‘Enrolment Request Period’), otherwise the enrolment request will automatically expire. If We are unable to confirm or decide to reject an enrolment request within the Enrolment Request Period, any amounts collected by Code At Random for the requested enrolment will be refunded to the concerned Student. When We confirm an enrolment requested by a Student, Code At Random will send the Student an email, text message or message via e-mail and the Application confirming such enrolment, depending on the selections you make via the Site, Application and Services.
    <br><br>
    The Course Fees payable will be displayed to the Student before the Student sends an enrolment request to Code At Random. Upon receipt of the Students enrolment request, Code At Random may initiate a pre-authorization and/or charge a nominal amount to Student’s Payment Method pursuant to the Payments Terms. If a requested enrolment is cancelled (before any tuitions are provided), any amounts collected by Code At Random will be refunded (except taxes charged) to such Student, depending on the selections the Student makes via the Site and Application, and any pre-authorization of Student's Payment Method will be released, if applicable.
    <br><br>
     
    
    Code At Random will collect the Course Fees from Students at the time of the enrolment request.
    <br><br>
    If, as a Student, you wish to cancel a confirmed enrolment made via the Site or the Application, after enrolment to the Course, the cancellation policy contained in the applicable Listing will apply to such cancellation provided that no refund will be made in respect of tuitions already provided. 
    <br><br>
    Details regarding refunds and cancellation policies are available via the Site and Application. Code At Random will initiate any refunds due pursuant to the Payments Terms.    
    
     <br><br>
     If We cancel a confirmed enrolment made via the Site, Services, and Application, (i) Code At Random will refund the Course Fees paid by the Student for such enrolment to the applicable Student pursuant to the Payments Terms which shall not exceed the total amount paid by the Student.
     <br><br>
    
    To know more about refunds, contact us on support@codeatrandom.com 
    <br><br>
    
    
    ----
    <?php include 'includes/footer(about).php'; ?>
    </body>
    
    </html>