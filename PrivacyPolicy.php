

<?php
require 'includes/common.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="node_modules/bootstrap-social/bootstrap-social.css">
    <link href="./node_modules/font-awesome/css/all.css" rel="stylesheet">
  <link href="./node_modules/font-awesome/css/fontawesome.css" rel="stylesheet">
  <link href="./node_modules/font-awesome/css/brands.css" rel="stylesheet">
  <link href="./node_modules/font-awesome/css/solid.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon_io/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon_io/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon_ioo/favicon-16x16.png">
<link rel="manifest" href="favicon_io/site.webmanifest">
    <title> Code@Random-Terms and Conditions</title>
</head>
<body>

<!--Login Modal-->
<?php include 'includes/login.php';?>
<!--SignUp Modal-->
<?php include 'includes/signup.php';?>
<nav class="navbar navbar-dark navbar-expand-sm fixed-top" id="navbarcr">
    <div class="container">
        <a class="navbar-brand col-sm-6" href="index.php"><img src="images/LOGO.png" height="50" width="50"></a>
</div>
</nav>
    <body style="font-family: Arial; line-height: 1.4em; padding-top:100px;">PRIVACY STATEMENT
    
     
    <br><br>
     
    
    SECTION 1 - WHAT DO WE DO WITH YOUR INFORMATION?
    <br><br>
    When you purchase something from our store, as part of the buying and selling process, we collect the personal information you give us such as your name, address and email address.    
    <br><br>
    When you browse our store, we also automatically receive your computer’s internet protocol (IP) address in order to provide us with information that helps us learn about your browser and operating system.
    <br><br>
    Email marketing (if applicable): With your permission, we may send you emails about our store, new products and other updates.
    <br><br>
     
    
    SECTION 2 - CONSENT
    <br><br>
    How do you get my consent?
    <br><br>
    When you provide us with personal information to complete a transaction, verify your credit card, place an order, arrange for a delivery or return a purchase, we imply that you consent to our collecting it and using it for that specific reason only.
    
     <br><br>
     If we ask for your personal information for a secondary reason, like marketing, we will either ask you directly for your expressed consent, or provide you with an opportunity to say no.
     <br><br>
    
    How do I withdraw my consent?
    <br><br>
    If after you opt-in, you change your mind, you may withdraw your consent for us to contact you, for the continued collection, use or disclosure of your information, at anytime, by contacting us at support@codeatrandom.com or mailing us at: 5, Khanderao Gate Jhansi.
    <br><br>
     
    
    SECTION 3 - DISCLOSURE
    <br><br>
    We may disclose your personal information if we are required by law to do so or if you violate our Terms of Service.
    <br><br>
    SECTION 4 - PAYMENT
    <br><br>
    We use Razorpay for processing payments. We/Razorpay do not store your card data on their servers. The data is encrypted through the Payment Card Industry Data Security Standard (PCI-DSS) when processing payment. Your purchase transaction data is only used as long as is necessary to complete your purchase transaction. After that is complete, your purchase transaction information is not saved.
    <br><br>
    Our payment gateway adheres to the standards set by PCI-DSS as managed by the PCI Security Standards Council, which is a joint effort of brands like Visa, MasterCard, American Express and Discover.
    <br><br>
    PCI-DSS requirements help ensure the secure handling of credit card information by our store and its service providers.
    <br><br>
    For more insight, you may also want to read terms and conditions of razorpay on https://razorpay.com
    <br><br>
    SECTION 5 - THIRD-PARTY SERVICES
    <br><br>
    In general, the third-party providers used by us will only collect, use and disclose your information to the extent necessary to allow them to perform the services they provide to us.
    <br><br>
    However, certain third-party service providers, such as payment gateways and other payment transaction processors, have their own privacy policies in respect to the information we are required to provide to them for your purchase-related transactions.
    <br><br>
    For these providers, we recommend that you read their privacy policies so you can understand the manner in which your personal information will be handled by these providers.
    <br><br>
    In particular, remember that certain providers may be located in or have facilities that are located a different jurisdiction than either you or us. So if you elect to proceed with a transaction that involves the services of a third-party service provider, then your information may become subject to the laws of the jurisdiction(s) in which that service provider or its facilities are located.
   <br><br>
   Once you leave our store’s website or are redirected to a third-party website or application, you are no longer governed by this Privacy Policy or our website’s Terms of Service.
    <br><br>
    Links <br><br>
    When you click on links on our store, they may direct you away from our site. We are not responsible for the privacy practices of other sites and encourage you to read their privacy statements.
    <br><br>
    SECTION 6 - SECURITY<br><br>
    
To protect your personal information, we take reasonable precautions and follow industry best practices to make sure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed.
<br><br>
SECTION 7 - COOKIES<br><br>
We use cookies to maintain session of your user. It is not used to personally identify you on other websites.
    <br><br>
    SECTION 8 - AGE OF CONSENT
    <br><br>
    
 By using this site, you represent that you are at least the age of majority in your state or province of residence, or that you are the age of majority in your state or province of residence and you have given us your consent to allow any of your minor dependents to use this site.
    <br><br>
    SECTION 9 - CHANGES TO THIS PRIVACY POLICY<br><br><br>

    If our store is acquired or merged with another company, your information may be transferred to the new owners so that we may continue to sell products to you.
    <br><br>
     
    
    QUESTIONS AND CONTACT INFORMATION
    <br><br>
    If you would like to: access, correct, amend or delete any personal information we have about you, register a complaint, or simply want more information contact our Privacy Compliance Officer at support@codeatrandom.com by mail at 5 Khanderao gate Jhansi.
    <br><br>
    
    ----
    <?php include 'includes/footer(about).php';?>
    </body>
    
    </html>