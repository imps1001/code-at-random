<div id="footer" class="generic_footer" style="margin-bottom: 10px;">
  <div class="container">
      <div class="max-width-container">             
          <div class="footer_container">
            <div class="row">
              <div class="col-sm-5 offset-md-1">
                <div class="footer-list footer-column ">
                 <h5>Code At Random</h5>
                 <div class="footer-list-item">
                   <a class="link" href="index.php">Home</a>
                 </div>
                 <div class="footer-list-item">
                   <a class="link" href="PrivacyPolicy.php">Privacy Policy</a>
                 </div>
                 <div class="footer-list-item">
                   <a class="link" href="index.php#courses">All Courses</a>
                 </div>
                 <div class="footer-list-item">
                   <a class="link" href="termsandconditions.php">Terms and Conditions</a>
                 </div>
                 <div class="footer-list-item">
                    <a class="link" href="contactus.php">Contact Us</a>
                 </div>
                 <div class="footer-list-item">
                    <a class="link" href="requirement_students.php">Fill Your Requirements</a>
                 </div>
            </div>
           </div>
         <div class="col-sm-5 offset-1 d-none d-md-block mr-auto">
             <div class="footer-list footer-column" style="float: right; margin-right: 200px;">
               <h5>Links</h5>
                 <div class="footer-list-item">
                   <a class="link" href="aboutus.php#mission_vision">Our Vision and Mission</a>
                 </div>
                 <div class="footer-list-item">
                    <a class="link" href="Loginlink">Login/Register</a>
                 </div>
                 <div class="footer-list-item">
                    <a class="link" href="index.php#courses">Java 9th ICSE</a>
                 </div>
                 <div class="footer-list-item">
                    <a class="link" href="index.php#courses"> All Courses</a>
                 </div>
                 <div class="footer-list-item">
                     <a class="link" href="termsandconditions.php">Privacy</a>
                 </div>
                 <div class="footer-list-item">
                     <a class="link" href="contactus.php">Refunds</a>
                 </div>
             </div>
       </div>
        </div>
      </div>
      </div>
      <hr color="grey">
      <div class="footer-bottom">
           <div class="row">
           <h5 class="col-sm-4 d-none d-lg-block">Connect With Us:-</h5>
           <h6 class="col-sm-3 col-md-4 d-none d-lg-block">Designed, Created And Managed By </h6>
           <div class="col-12 col-md-4 ml-auto order-sm-last" id='copyright' style="margin-right:0px; color: white; float:right;">
                  <h6><strong>CODE AT RANDOM (OPC) Private Limited</strong></h6>
          </div>
          </div>
          <div class="row">
          <div id="social-media-container" style="margin-left: 0px;" class="col-12 col-sm-4 col-md-4 order-sm-first">
                  <a class="btn btn-social-icon btn-twitter" href="#" target="_blank"><i class="fab fa-telegram"></i></a>
                  <a class="btn btn-social-icon btn-facebook" href="http://www.facebook.com/codeatrandom" target="_blank"><i class="fab fa-facebook-square"></i></a>
                  <a class="btn btn-social-icon btn-instagram" href="https://www.instagram.com/codeatrandom/" target="_blank"><i class="fab fa-instagram-square"></i></a>
                  <a class="btn btn-social-icon btn-google" href="https://www.youtube.com/channel/UCFykORvcikeYASRLNARaj4g" target="_blank"><i class="fab fa-youtube-square"></i></a>
                  <a style="color: white;" class="btn btn-social-icon btn-adn" href="mailto:support@codeatrandom.com" target="_blank"><i class="fas fa-envelope-square"></i></a>
          </div>
          <div class="col-sm-4 col-md-3 d-none d-lg-block" >
          <h5 class="ml-3">Pragati Sahu</h5>
          </div>
          <div class="col-12 col-sm-4 col-md-4 offset-md-1" style="float: right;">
          <h6 class="mt-2">Copyright &copy; 2020-21 All Rights Reserved</h6>
          </div>
          </div>
                <div class="clear">
                </div>
          </div>
     </div>
  </div>
</div>